class ApiController < ApplicationController
    
  before_filter :authenticate_user!,  :only => [:genius]
  
  def featured
    @restaurants=nil
    @offers=nil
    render :json => {:restaurants=>@restaurants, :offers=>@offers}
  end

  def menu_for_restaurant
    @menu=nil
    render :json => @menu
  end
  
  def user_login
    user = User.find_by_email(params[:user][:email])
    user.valid_password?(params[:user][:password])
    
  end
  
  def genius
    @closer_restaurants=APIhelper.get_restaurants_near_latlong(LatLong.new(lat:params[:lat], long:params[:long]))
    render :json => {:restaurants=>@closer_restaurants}
  end  
  
  class APIhelper
   
    def self.get_restaurants_near_latlong(latlong)
      return restaurants= Restaurant.all.sort_by{|restaurant| squared_distance_between_latlongs(restaurant.lat_long, latlong)}[0..10]
    end

    def self.squared_distance_between_latlongs(first_latlong, second_latlong)
      latdis=(first_latlong.lat-second_latlong.lat)
      longdis=(first_latlong.long-second_latlong.long)
      return (latdis*latdis)+(longdis*longdis)
    end
    
  end

end