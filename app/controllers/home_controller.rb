class HomeController < ApplicationController
  
  def index
    @restaurants=Restaurant.all
     respond_to do |format|
        format.html {render}
        format.json { render :json => @restaurants}
      end
  end
  
end