class SearchController < ApplicationController

  def index
    respond_to do |format|
      format.html {render}
      format.json { render :json => "search page"}
    end  
  end
  
end