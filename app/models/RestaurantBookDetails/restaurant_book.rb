class RestaurantBook
  include Mongoid::Document
  
  #type(chais/individual/malls)

  #string type fields
  [:website_url, :contact_email, :facebook_page_url].each do |field_name|
    field field_name, type: String
  end
  
  #embeds_many
  {:services=>String, :facilities=>String, :image_urls=>String, :social_links=>String, :offers=>Offer}.each do |k,v|
    embeds_many k, :class_name => v
  end
  
end