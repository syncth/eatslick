class Combo
  include Mongoid::Document
  field :name, :type=>String
  filed :desc, :type=>String
  has_many    :items
  embedded_in :menu
end