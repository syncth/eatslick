class Menu
  include Mongoid::Document
  embedded_in :restaurant
  embeds_many :combos
  embeds_many :specials
  has_many :items
end