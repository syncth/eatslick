class Special
  include Mongoid::Document
  field :title, type: String
  embedded_in :menu
  belongs_to :item
end