class Item
  include Mongoid::Document
  belongs_to :menu, index: true
  field :name, type: String
end