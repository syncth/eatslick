class RestaurantLatLong
  include Mongoid::Document
  field :lat, type:Float
  field :long, type:Float
  embedded_in :restaurant_address
end

class RestaurantContactNumbers
  include Mongoid::Document
  field :mobile, type:String
	field :landLine, type:String
	embedded_in :restaurant_address
end

class RestaurantAddress
  include Mongoid::Document
  embedded_in :restaurant
	field :contact_email, type:String
	field :city, type:String
  embeds_one :restaurant_lat_long
  field :state, type:String
  field :country, type:String
  embeds_one :restaurant_contact_number
end