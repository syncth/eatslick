class AboutRestaurant
  include MongoMapper::Document
  key :tagline, String
	key :desc, String
	embedded_in :Restaurant
end

class RestaurantSocialLink
  include MongoMapper::Document
  key :network, String
  key :link, String
end

class Restaurant
  include MongoMapper::Document
  key :name, String
  #belongs_to identity(has_and_belongs_to_many)->RestaurantIdentity
  embeds_one :about_restaurant
  many :restaurant_type_ids, Mongo::ObjectID
  many :restaurant_cuisine_ids, Mongo::ObjectID
  many :restaurant_service_ids, Mongo::ObjectID
  many :ambience, Mongo::ObjectID
  key :tags, Array
  embeds_many :restaurant_social_links
  key :teamForce, Integer
  key :website, String
  key :costLegue, String
  #timings??
  embeds_one :restaurant_address
  has_many :transactions
  has_one :restaurant_book
  belongs_to :restaurant_chain
end