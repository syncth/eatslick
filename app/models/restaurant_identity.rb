class RestaurantIdentity
  include Mongoid::Document
  field                   :name,        type:String
  field                   :desc,        type:String
  has_and_belongs_to_many :restaurants, type:String
end
