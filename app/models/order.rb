class Order
  include Mongoid::Document
  
  belongs_to  :restaurant
  belongs_to  :user
  field       :status,      type:String
  has_one     :transaction
end
