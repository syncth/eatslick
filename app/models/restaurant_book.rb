class Combo
  include Mongoid::Document
  field :name, type:String
  has_and_belongs_to_many :items, inverse_of:nil
  embedded_in :restaurant_book
end

class ComplimentingItem
  include Mongoid::Document
  field :name, type:String
  field :cost, type:Float
  field :desc, type:String
  embedded_in :restaurant_book
end

class Item
  include Mongoid::Document
  field :name, type: String
  field :cost, type: Float
  embedded_in :restaurant_book
  field :cuisine, type:String
  field :ingredients, type:Array
  belongs_to :category
  field :complimenting_item_ids, type:Array

  def complimenting_items
    if (complimenting_item_ids==nil)
      complimenting_item_ids=[]
    end
    if(@complimenting_items==nil)
      @complimenting_items=restaurant_book.complimenting_items.select{|i| complimenting_item_ids.include?i.id}
    end
    def @complimenting_items.delete_all
      @complimenting_items=[]
    end
    return @complimenting_items
  end
  
  def complimenting_items=(mcomplimenting_items)
    @complimenting_items=mcomplimenting_items
    complimenting_item_ids=complimenting_items.collect{|i| i.id}
  end
  
  def before_save
    complimenting_item_ids=complimenting_items.collect{|i| i.id}
  end
  
  #has_and_belongs_to_many :upselling_items, class_name: "Item", inverse_of:nil
  
end

class Offer
  include Mongoid::Document
  embedded_in :restaurant_book
  field :name, type:String
  field :desc, type:String
end

class Special
  include Mongoid::Document
  embedded_in :restaurant_book
  field :name, type:String
end

class ItemCategory
  include Mongoid::Document
  has_many :items
  belongs_to :restaurant_menu
end

class RestaurantMenu
  include Mongoid::Document
  has_many :item_category
  embedded_in :restaurant_book
end

class RestaurantBook
  include Mongoid::Document
  belongs_to :restaurant
  embeds_many :offers
  embeds_many :specials
  embeds_many :combos
  embeds_many :items
  embeds_many :complimenting_items
  embeds_one :restaurant_menu
  
  def before_save
    items.each do |item|
      (item.complimenting_item_ids-complimenting_items).each do |i|
        item.complimenting_item_ids.delete(i)
      end
    end
  end
  
end