class RestaurantMetadata
  include Mongoid::Document
  
  [:type, :cusine, :desc, :tag_line, :ambience].each do |field_name|
    field field_name, type: String
  end
  
end