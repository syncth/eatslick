class Restaurant

  include Mongoid::Document
  #type(chais/individual/malls)
  
  #restaurant meta data
  has_one :restaurant_metadata
  
  #string type fields
  [
    :name,
    :status,
    :currency,
    :cost_legue,
    :logo_url
  ].each do |field_name|
    field field_name, type: String
  end
  
  #integer type fields
  [
    :costForTwo, 
    :rating, 
    :team_force
  ].each do |field_name|
    field field_name, type: Integer
  end

  #embeds_one
  {
    :lat_long => 'LatLong'
  }.each do |k,v|
    embeds_one k, :class_name => v
  end
  
  
  #has_one
  {
    :restaurant_book => RestaurantBook
  }.each do |k,v|
    has_one k, :class_name  => v
  end
  
  #services
    #online
    #pickup/delivery
    #corporate/bulk
    #special_events
    #occations
    #dinein
  #facilities
    #wifi
    #bar
    #parking
    #cash/card
    #banquet
   
    
    #printer_configuration
    #kot_printer
     # lan_printer
    #  wifi printer
    #  cloud printer
    
    #(payment_details)
      #cash/card(payment gateway)
      #bank_account/checking account setup
      #floor_plan
    
  # role_management
  #     name_of_employees
  #     designation
  #     id
  #     shifts/timings(ROTA creation)
  #     table_number_alloting
  #     contact_info
  #     address details
  #     skillset
  #     languages_spoken
  #     ethnic orign
  
end