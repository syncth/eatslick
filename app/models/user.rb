class User
  include MongoMapper::Document
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :token_authenticatable
         :recoverable, :rememberable, :trackable, :validatable

  # Database authenticatable
  key :email,String
  key :encrypted_password,String
  
  validates_presence_of :email
  validates_presence_of :encrypted_password
  
  ## Recoverable
  key :reset_password_token,String
  key :reset_password_sent_at,Time
  
  ## Rememberable
  key :remember_created_at,Time
  
  ## Trackable
  key :sign_in_count,Integer
  key :current_sign_in_at,Time
  key :last_sign_in_at,Time
  key :current_sign_in_ip, String
  key :last_sign_in_ip,String
  
  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable
  
  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time
  
  ## Token authenticatable
  # field :authentication_token, :type => String
  key :first_name,String
  key :last_name,String
  key :email,String
  key :password,String
  key :isActive,Boolean
  
  many  :favorite_restaurants,   :class_name=>:Restaurant
  many  :favorite_items,         :class_name=>:Item
  many  :transaction_history,    :class_name=>:Transaction
  many  :placed_orders,          :class_name=>:Oder

end