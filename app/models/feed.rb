class FeedEntity
  include Mongoid::Document
  field :name,      type:String
  field :type,      type:String
  field :identity,  type:String
  embeded_in :feed_entity
end

class FeedDeed
  include Mongoid::Document
  field :name, type:String
  field :desc, type:String
  embeded_in :feed_entity
end

class Feed
  include Mongoid::Document
  embeds_one :sourceEntity,       class_name: :FeedEntity
  embeds_one :destinationEntity,  class_name: :FeedEntity
  embeds_one :deed,               class_name: :FeedDeed
end
