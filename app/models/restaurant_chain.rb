class RestaurantChain
  include Mongoid::Document
  field :name, type:String
  has_many :restaurants
end
